import UIKit
import Foundation

public class UITitleView: UIView {

    public let titleLabel: UILabel = UILabel()
    public let subtitleLabel: UILabel = UILabel()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.layout()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.layout()
    }
    
    private func layout() {
        self.titleLabel.textAlignment = .center
        self.subtitleLabel.textAlignment = .center
        self.titleLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        self.subtitleLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        self.titleLabel.adjustsFontSizeToFitWidth = true
        self.subtitleLabel.adjustsFontSizeToFitWidth = true
        self.subtitleLabel.lineBreakMode = .byTruncatingMiddle
        self.subtitleLabel.minimumScaleFactor = 0.75
        self.addSubview(self.titleLabel)
        self.addSubview(self.subtitleLabel)
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 2.0).isActive = true
        self.titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 2.0).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -2.0).isActive = true
        self.titleLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.66).isActive = true
        self.subtitleLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: -2.0).isActive = true
        self.subtitleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 2.0).isActive = true
        self.subtitleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -2.0).isActive = true
        self.subtitleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -2.0).isActive = true
    }
}
