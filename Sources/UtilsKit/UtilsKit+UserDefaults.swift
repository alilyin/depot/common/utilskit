import Foundation

public extension UserDefaults {
    
    func reset(for bundle: Bundle) {
        guard let domain = bundle.bundleIdentifier else {return}
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}

