import Foundation

public extension Bundle {
    
    var version: String {
        guard
            let dictionary = self.infoDictionary,
            let version = dictionary["CFBundleShortVersionString"] as? String
        else {
            return ""
        }
        return version
    }
    
    var fullversion: String {
        guard
            let dictionary = self.infoDictionary,
            let build = dictionary["CFBundleVersion"] as? String,
            let version = dictionary["CFBundleShortVersionString"] as? String
        else {
            return ""
        }
        return "\(version).\(build)"
    }
}
