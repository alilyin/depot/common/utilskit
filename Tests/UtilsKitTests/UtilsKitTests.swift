import XCTest
@testable import UtilsKit

final class UtilsKitTests: XCTestCase {
    
    func testUIViewAddSubview() throws {
        let view: UIView = UIView()
        let subview: UIView = UIView()
        view.addSubView(subview, wiith: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0))
        XCTAssert(view.subviews.first! == subview)
        XCTAssert(subview.superview == view)
    }
    
    func testBundleVersion() throws {
        let bundle: Bundle = Bundle()
        XCTAssert(bundle.version == "")
        XCTAssert(bundle.fullversion == "")
    }
    
    func testUITitleView() throws {
        let titleView: UITitleView = UITitleView()
        titleView.titleLabel.text = "testtitle"
        titleView.subtitleLabel.text = "testsubtitle"
        XCTAssert(titleView.titleLabel.text == "testtitle")
        XCTAssert(titleView.subtitleLabel.text == "testsubtitle")
    }
    
    func testUserDefaults() throws {
        UserDefaults.standard.setValue("aaa", forKey: "bbb")
        UserDefaults.standard.synchronize()
        XCTAssert(UserDefaults.standard.object(forKey: "bbb") as! String == "aaa")
        UserDefaults.standard.reset(for: Bundle.main)
        XCTAssert(UserDefaults.standard.object(forKey: "bbb") == nil)
    }

    static var allTests = [
        ("testUIViewAddSubview", testUIViewAddSubview),
        ("testBundleVersion", testBundleVersion),
        ("testUITitleView", testUITitleView),
        ("testUserDefaults", testUserDefaults),
    ]
}
