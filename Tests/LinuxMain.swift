import XCTest

import UtilsKitTests

var tests = [XCTestCaseEntry]()
tests += UtilsKitTests.allTests()
XCTMain(tests)
